﻿using BusinessLogic.DataAccess;
using BusinessLogic.Dto;
using BusinessLogic.Factory;
using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Services
{
    public class EmployeeService
    {

        public List<EmployeeDto> GetEmployees()
        {
            EmployeeDAL employeeDAL = new EmployeeDAL();
            List<EmployeeDto> employees = employeeDAL.FindAll().Result;
            List<EmployeeDto> employeesWithAnnualSalary = new List<EmployeeDto>();
            foreach(EmployeeDto employee in employees)
            {
                IAnnualSalaryEmployees annualSalaryEmployees = AnnualSalaryFactory.Build(employee);
                if(annualSalaryEmployees != null)
                {
                    employee.AnnualSalary = annualSalaryEmployees.CalculateAnnualSalary();
                    employeesWithAnnualSalary.Add(employee);
                }
            }
            return employeesWithAnnualSalary;
        }

        public EmployeeDto GetEmployee(long id)
        {
            EmployeeDAL employeeDAL = new EmployeeDAL();
            EmployeeDto employee = employeeDAL.FindEmployee(id).Result;
            IAnnualSalaryEmployees annualSalaryEmployees = AnnualSalaryFactory.Build(employee);
            if (annualSalaryEmployees != null)
            {
                employee.AnnualSalary = annualSalaryEmployees.CalculateAnnualSalary();
            }
            return employee;
        }

    }
}
