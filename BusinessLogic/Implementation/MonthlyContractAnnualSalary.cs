﻿using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Implementation
{
    public class MonthlyContractAnnualSalary : IAnnualSalaryEmployees
    {
        private readonly double salary;

        public MonthlyContractAnnualSalary(double salary)
        {
            this.salary = salary;
        }

        public double CalculateAnnualSalary()
        {
            double annualSalary = salary * 12;
            return annualSalary;
        }
    }
}
