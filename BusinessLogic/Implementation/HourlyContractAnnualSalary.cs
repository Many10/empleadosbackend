﻿using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Implementation
{
    public class HourlyContractAnnualSalary : IAnnualSalaryEmployees
    {
        private readonly double salary;

        public HourlyContractAnnualSalary(double salary)
        {
            this.salary = salary;
        }

        public double CalculateAnnualSalary()
        {
            double annualSalary = 120 * salary * 12;
            return annualSalary;
        }
    }
}
