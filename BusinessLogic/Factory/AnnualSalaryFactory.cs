﻿using BusinessLogic.Constants;
using BusinessLogic.Dto;
using BusinessLogic.Implementation;
using BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Factory
{
    public class AnnualSalaryFactory
    {

        public static IAnnualSalaryEmployees Build(EmployeeDto employee)
        {
            if(ContractConstants.HOURLY_CONTRACT.Equals(employee.ContractTypeName)) {
                return new HourlyContractAnnualSalary(employee.HourlySalary);
            } else if(ContractConstants.MONTHLY_CONTRACT.Equals(employee.ContractTypeName))
            {
                return new MonthlyContractAnnualSalary(employee.MonthlySalary);
            }
            return null;
        }

    }
}
