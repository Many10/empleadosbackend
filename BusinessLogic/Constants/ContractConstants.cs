﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Constants
{
    public class ContractConstants
    {
        public static readonly string HOURLY_CONTRACT = "HourlySalaryEmployee";
        public static readonly string MONTHLY_CONTRACT = "MonthlySalaryEmployee";
    }
}
