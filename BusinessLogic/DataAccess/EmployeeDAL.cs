﻿using BusinessLogic.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.DataAccess
{
    public class EmployeeDAL
    {
        private const string URL = "http://masglobaltestapi.azurewebsites.net/";
        private const string RESOURCE = "api/Employees/";


        public async Task<List<EmployeeDto>> FindAll()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                List<EmployeeDto> employees = null;
                HttpResponseMessage response = client.GetAsync(RESOURCE).Result;
                if (response.IsSuccessStatusCode)
                {
                    employees = await response.Content.ReadAsAsync<List<EmployeeDto>>();
                }
                return employees;
            }
        }

        public async Task<EmployeeDto> FindEmployee(long id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                List<EmployeeDto> employees = null;
                EmployeeDto employee = null;
                HttpResponseMessage response = client.GetAsync(RESOURCE).Result;
                if (response.IsSuccessStatusCode)
                {
                    employees = await response.Content.ReadAsAsync<List<EmployeeDto>>();
                    employee = employees.Where(e => e.Id == id).FirstOrDefault();
                }
                return employee;
            }
        }


    }
}
