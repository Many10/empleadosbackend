﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using BusinessLogic.Dto;

namespace BusinessLogic.DataAccess.Tests
{
    [TestClass()]
    public class EmployeeDALTests
    {
        [TestMethod()]
        public void FindAllTest()
        {
            EmployeeDAL employeeDal = new EmployeeDAL();
            List<EmployeeDto> listEmployees = employeeDal.FindAll().Result;
            Assert.IsTrue(listEmployees.Count == 2);
        }
    }
}