﻿using BusinessLogic.Dto;
using BusinessLogic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MasGlobalTest.Controllers
{
    [EnableCors("*", "*", "*")]
    public class EmployeeController : ApiController
    {

        [HttpGet]
        [Route("api/Employee/getEmployees")]
        public IHttpActionResult GetEmployees()
        {
            EmployeeService service = new EmployeeService();
            List<EmployeeDto> employees = service.GetEmployees();
            if (employees == null && employees.Count <= 0)
            {
                return NotFound();
            }
            return Ok(employees);
        }

        [HttpGet]
        [Route("api/Employee/getEmployee/{id}")]
        public IHttpActionResult GetEmployeeById(long id)
        {
            EmployeeService service = new EmployeeService();
            EmployeeDto employee = service.GetEmployee(id);
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }

    }
}
